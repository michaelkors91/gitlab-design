window.__imported__ = window.__imported__ || {};
window.__imported__["ee#2122-crossprojectpipelines@2x/layers.json.js"] = [
	{
		"objectId": "017872AB-F3AD-418B-B839-5CDD15100A4C",
		"kind": "artboard",
		"name": "screen",
		"originalName": "screen",
		"maskFrame": null,
		"layerFrame": {
			"x": -334,
			"y": 68,
			"width": 1729,
			"height": 817
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-screen-mde3odcy.png",
			"frame": {
				"x": -334,
				"y": 68,
				"width": 1729,
				"height": 817
			}
		},
		"children": [
			{
				"objectId": "D1F67D2B-EBAB-4DCA-9402-1834BF2CF338",
				"kind": "group",
				"name": "supergraph",
				"originalName": "supergraph",
				"maskFrame": null,
				"layerFrame": {
					"x": -528,
					"y": 358,
					"width": 3056,
					"height": 278
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "64894F82-5C8E-4920-9B82-245F4104729B",
						"kind": "group",
						"name": "upstream",
						"originalName": "upstream",
						"maskFrame": null,
						"layerFrame": {
							"x": -528,
							"y": 358,
							"width": 769,
							"height": 194
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-upstream-njq4otrg.png",
							"frame": {
								"x": -528,
								"y": 358,
								"width": 769,
								"height": 194
							}
						},
						"children": [
							{
								"objectId": "A2582397-CF00-4420-B15F-C72EA9710AC7",
								"kind": "group",
								"name": "issue_boards_organisms_list",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": -16,
									"y": 358,
									"width": 240,
									"height": 96
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-qti1odiz.png",
									"frame": {
										"x": -16,
										"y": 358,
										"width": 240,
										"height": 96
									}
								},
								"children": [
									{
										"objectId": "20B6858E-CB82-488F-953A-60884AED080F",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": -16,
											"y": 358,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-mjbcnjg1.png",
											"frame": {
												"x": -16,
												"y": 358,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "CC3E1CE4-E15E-4982-9D7F-88235BEA92D0",
												"kind": "group",
												"name": "Group_12",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": -0.9999999999999982,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-q0mzrtfd.png",
													"frame": {
														"x": -0.9999999999999982,
														"y": 418,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "5D5C30D2-1EAE-481B-A5E6-C1CC5A8B99E7",
														"kind": "group",
														"name": "Group_10",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": -0.375,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nuq1qzmw.png",
															"frame": {
																"x": -0.375,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "54CD35D7-01D0-4697-AF07-22CFFBD89ABF",
																"kind": "group",
																"name": "icon_check_s",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": -0.375,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "FD6BFDE8-4E2D-4725-A1F3-5F4BD3D25611",
																		"kind": "group",
																		"name": "Group",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": -0.375,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-rkq2qkze.png",
																			"frame": {
																				"x": -0.375,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "F9E706C7-9AEA-4C0B-8B8D-4322343767A3",
												"kind": "text",
												"name": "title",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 378,
													"width": 89,
													"height": 13
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-cleanup",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-rjlfnza2.png",
													"frame": {
														"x": 0,
														"y": 378,
														"width": 89,
														"height": 13
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "F6BF7A91-50F6-465B-92D1-5579E36AF567",
								"kind": "group",
								"name": "issue_boards_organisms_list1",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": -272,
									"y": 358,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-rjzcrjdb.png",
									"frame": {
										"x": -272,
										"y": 358,
										"width": 240,
										"height": 97
									}
								},
								"children": [
									{
										"objectId": "73109DFE-E02E-4F0A-9287-980DE2092E5D",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible1",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": -272,
											"y": 358,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-nzmxmdle.png",
											"frame": {
												"x": -272,
												"y": 358,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "0DB48544-8832-48AA-92A1-0D91411BA0AC",
												"kind": "group",
												"name": "Group_121",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": -257,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mercndg1.png",
													"frame": {
														"x": -257,
														"y": 418,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "4DC84434-12E4-4816-B40E-EAF3B2718CCA",
														"kind": "group",
														"name": "Group_101",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": -256.375,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nerdodq0.png",
															"frame": {
																"x": -256.375,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "E920B904-B3DE-4252-A2B8-837539F1BCDA",
																"kind": "group",
																"name": "icon_check_s1",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": -256.375,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "0900140B-263F-47D6-B12C-49B9DCC56950",
																		"kind": "group",
																		"name": "Group1",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": -256.375,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-mdkwmde0.png",
																			"frame": {
																				"x": -256.375,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "74826672-250E-4E28-9666-6FB30A1BFE79",
												"kind": "text",
												"name": "title1",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": -256,
													"y": 378,
													"width": 62,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-test",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-nzq4mjy2.png",
													"frame": {
														"x": -256,
														"y": 378,
														"width": 62,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "253E35FB-8030-4442-86BC-9E3873C73E55",
								"kind": "group",
								"name": "issue_boards_organisms_list2",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": -528,
									"y": 358,
									"width": 240,
									"height": 194
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-mjuzrtm1.png",
									"frame": {
										"x": -528,
										"y": 358,
										"width": 240,
										"height": 194
									}
								},
								"children": [
									{
										"objectId": "BC3FF969-8984-4EF1-BF00-21D042DC234B",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible2",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": -528,
											"y": 358,
											"width": 240,
											"height": 182
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-qkmzrky5.png",
											"frame": {
												"x": -528,
												"y": 358,
												"width": 240,
												"height": 182
											}
										},
										"children": [
											{
												"objectId": "94EB25D9-0A11-4119-A453-663097297A2B",
												"kind": "group",
												"name": "Group_122",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": -513,
													"y": 516,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-otrfqji1.png",
													"frame": {
														"x": -513,
														"y": 516,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "99C1D8A9-9C18-40F1-8118-0EC65D86C4B8",
														"kind": "group",
														"name": "Group_102",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": -512.375,
															"y": 516,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-otldmuq4.png",
															"frame": {
																"x": -512.375,
																"y": 516,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "1ED51566-CFB2-4C63-82BF-EC6E26966C15",
																"kind": "group",
																"name": "icon_check_s2",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": -512.375,
																	"y": 516,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "E469CF2E-675C-47C4-9B72-FC3A74A93072",
																		"kind": "group",
																		"name": "Group2",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": -512.375,
																			"y": 516,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-rtq2oung.png",
																			"frame": {
																				"x": -512.375,
																				"y": 516,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "B186BF17-7C18-4B38-B1C0-E3204FDD8EC4",
												"kind": "group",
												"name": "Group_123",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": -528,
													"y": 467,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-qje4nkjg.png",
													"frame": {
														"x": -528,
														"y": 467,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "70A16FE0-D6B4-45A1-955A-37985B6FD567",
														"kind": "group",
														"name": "Group_103",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": -512.375,
															"y": 467,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nzbbmtzg.png",
															"frame": {
																"x": -512.375,
																"y": 467,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "02ADEF10-86F3-4B49-BCA2-245BE1F754E8",
																"kind": "group",
																"name": "icon_check_s3",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": -512.375,
																	"y": 467,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "72FAEF0B-9255-4D1A-BE8A-1B4EB8954C92",
																		"kind": "group",
																		"name": "Group3",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": -512.375,
																			"y": 467,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-nzjgquvg.png",
																			"frame": {
																				"x": -512.375,
																				"y": 467,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "170BD7CC-3A70-41EA-B032-8BADDAB598E2",
												"kind": "group",
												"name": "Group_124",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": -528,
													"y": 418,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mtcwqkq3.png",
													"frame": {
														"x": -528,
														"y": 418,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "9518ACE5-794F-4B75-8B58-8D3CD827E4C0",
														"kind": "group",
														"name": "Group_104",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": -512.375,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-otuxoefd.png",
															"frame": {
																"x": -512.375,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "75E127FC-4E5F-43F2-BD7F-C76BC31565BF",
																"kind": "group",
																"name": "icon_check_s4",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": -512.375,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "8EF528A3-F1F7-4BA7-8D15-513282E9520E",
																		"kind": "group",
																		"name": "Group4",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": -512.375,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-oevgnti4.png",
																			"frame": {
																				"x": -512.375,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "263ABEFC-7DB3-473C-8E00-CB28507F8D94",
												"kind": "text",
												"name": "title2",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": -512,
													"y": 378,
													"width": 35,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Tests",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-mjyzqujf.png",
													"frame": {
														"x": -512,
														"y": 378,
														"width": 35,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "462DFD3F-E46B-4955-92EF-65C16E356E9F",
						"kind": "group",
						"name": "usloading",
						"originalName": "usloading",
						"maskFrame": null,
						"layerFrame": {
							"x": -16,
							"y": 358,
							"width": 257,
							"height": 97
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-usloading-ndyyreze.png",
							"frame": {
								"x": -16,
								"y": 358,
								"width": 257,
								"height": 97
							}
						},
						"children": [
							{
								"objectId": "0B0A74EE-329B-46F4-9BF4-7C3926DEC48D",
								"kind": "group",
								"name": "issue_boards_organisms_list3",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": -16,
									"y": 358,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "3F012A5F-1AE1-4F34-90DA-3630B5798FE0",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible3",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": -16,
											"y": 358,
											"width": 240,
											"height": 48
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-m0ywmtjb.png",
											"frame": {
												"x": -16,
												"y": 358,
												"width": 240,
												"height": 48
											}
										},
										"children": [
											{
												"objectId": "EF47042A-BFEF-408C-9FB6-91B56AA42469",
												"kind": "group",
												"name": "background1",
												"originalName": "background",
												"maskFrame": null,
												"layerFrame": {
													"x": -16,
													"y": 358,
													"width": 240,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-background-ruy0nza0.png",
													"frame": {
														"x": -16,
														"y": 358,
														"width": 240,
														"height": 48
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "DBD2A962-9C00-498A-8223-8D1A25DF4B15",
										"kind": "group",
										"name": "Group_105",
										"originalName": "Group 10",
										"maskFrame": null,
										"layerFrame": {
											"x": 0,
											"y": 418,
											"width": 208,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "52531051-D559-4801-8F66-ED86A90C036C",
												"kind": "group",
												"name": "icon_check_s5",
												"originalName": "icon-check-s",
												"maskFrame": null,
												"layerFrame": {
													"x": 0,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "C9A80C8F-CAD3-4B6F-B172-1602A6F54479",
														"kind": "group",
														"name": "Group5",
														"originalName": "Group",
														"maskFrame": null,
														"layerFrame": {
															"x": 0,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group-qzlbodbd.png",
															"frame": {
																"x": 0,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "9143684D-27BD-4506-A280-BB6B33E88794",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": -16,
											"y": 358,
											"width": 240,
											"height": 97
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-ote0mzy4.png",
											"frame": {
												"x": -16,
												"y": 358,
												"width": 240,
												"height": 97
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "DC34DF0B-0BAB-47DF-916A-3CB778D6DBA5",
						"kind": "group",
						"name": "downstream",
						"originalName": "downstream",
						"maskFrame": null,
						"layerFrame": {
							"x": 1503,
							"y": 442,
							"width": 1025,
							"height": 194
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-downstream-remznerg.png",
							"frame": {
								"x": 1503,
								"y": 442,
								"width": 1025,
								"height": 194
							}
						},
						"children": [
							{
								"objectId": "26406C1D-D29F-4EAD-9DE7-49D1690A46AC",
								"kind": "group",
								"name": "issue_boards_organisms_list4",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 2288,
									"y": 442,
									"width": 240,
									"height": 84
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-mjy0mdzd.png",
									"frame": {
										"x": 2288,
										"y": 442,
										"width": 240,
										"height": 84
									}
								},
								"children": [
									{
										"objectId": "B4584DC8-2FBB-420F-8987-7D74A9216C88",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible4",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 2304,
											"y": 459,
											"width": 203,
											"height": 51
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-qjq1odre.png",
											"frame": {
												"x": 2304,
												"y": 459,
												"width": 203,
												"height": 51
											}
										},
										"children": [
											{
												"objectId": "EB6B383D-E625-4AA9-B71D-3BC56FD26A20",
												"kind": "group",
												"name": "Group_125",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 2289,
													"y": 576,
													"width": 239,
													"height": 57
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-rui2qjm4.png",
													"frame": {
														"x": 2289,
														"y": 576,
														"width": 239,
														"height": 57
													}
												},
												"children": []
											},
											{
												"objectId": "07EA760A-7E6D-4286-9885-CF878BF3BA07",
												"kind": "group",
												"name": "Group_126",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 2304,
													"y": 459,
													"width": 203,
													"height": 51
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mddfqtc2.png",
													"frame": {
														"x": 2304,
														"y": 459,
														"width": 203,
														"height": 51
													}
												},
												"children": [
													{
														"objectId": "6B7133DD-F748-4DAE-8A73-26CEC3460D87",
														"kind": "group",
														"name": "Group_106",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 2303.625,
															"y": 459,
															"width": 204,
															"height": 51
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nki3mtmz.png",
															"frame": {
																"x": 2303.625,
																"y": 459,
																"width": 204,
																"height": 51
															}
														},
														"children": [
															{
																"objectId": "E4AFD558-4A11-4471-A1CD-9F48C05DB803",
																"kind": "group",
																"name": "global_components_other_avatar_",
																"originalName": "global-components/other/avatar/",
																"maskFrame": null,
																"layerFrame": {
																	"x": 2302.625,
																	"y": 459,
																	"width": 25,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "F2E16021-B78B-4F55-8EFD-FB7196DC53C1",
																		"kind": "group",
																		"name": "global_components_other_avatar_modifier_user_avatar",
																		"originalName": "global-components/other/avatar/modifier/user-avatar",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 2303,
																			"y": 459,
																			"width": 24,
																			"height": 24
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "1FB31AC5-B985-46FF-B482-E8F73EC659AE",
																				"kind": "group",
																				"name": "group_icon",
																				"originalName": "group-icon",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 2303,
																					"y": 459,
																					"width": 25,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-group_icon-muzcmzfb.png",
																					"frame": {
																						"x": 2303,
																						"y": 459,
																						"width": 25,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	}
																]
															},
															{
																"objectId": "CFD143C4-036D-4F34-9CA7-42F14245D7BF",
																"kind": "group",
																"name": "icon_check_s6",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 2303.625,
																	"y": 459,
																	"width": 25,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "24911CF4-3B54-4C2A-A248-C8C86DCD434B",
																		"kind": "group",
																		"name": "Group6",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 2304,
																			"y": 459,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-mjq5mtfd.png",
																			"frame": {
																				"x": 2304,
																				"y": 459,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "067A07FE-23A7-4C16-943E-14CE58F126A8",
								"kind": "group",
								"name": "issue_boards_organisms_list5",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 2032,
									"y": 442,
									"width": 240,
									"height": 96
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-mdy3qta3.png",
									"frame": {
										"x": 2032,
										"y": 442,
										"width": 240,
										"height": 96
									}
								},
								"children": [
									{
										"objectId": "08261423-D564-4928-B140-D31B74168658",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible5",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 2032,
											"y": 442,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-mdgynje0.png",
											"frame": {
												"x": 2032,
												"y": 442,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "00B65BF3-5932-4B2A-97EB-23FE714C73A3",
												"kind": "group",
												"name": "Group_127",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 2047,
													"y": 502,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mdbcnjvc.png",
													"frame": {
														"x": 2047,
														"y": 502,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "4D8FAE04-7C84-4184-8127-36192D38163A",
														"kind": "group",
														"name": "Group_107",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 2047.625,
															"y": 502,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-neq4rkff.png",
															"frame": {
																"x": 2047.625,
																"y": 502,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "5C08718E-9DB0-425F-9E61-A57F072CCBD4",
																"kind": "group",
																"name": "icon_check_s7",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 2047.625,
																	"y": 502,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "A92FAFA6-6F8D-48F1-A3FF-FCB51ED2D5AE",
																		"kind": "group",
																		"name": "Group7",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 2047.625,
																			"y": 502,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-qtkyrkfg.png",
																			"frame": {
																				"x": 2047.625,
																				"y": 502,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "B26244CD-F5AB-438B-9233-9F60CCC6A03A",
												"kind": "text",
												"name": "title3",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 2048,
													"y": 462,
													"width": 89,
													"height": 13
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-cleanup",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-qji2mjq0.png",
													"frame": {
														"x": 2048,
														"y": 462,
														"width": 89,
														"height": 13
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "473217C0-998B-486F-8CDA-DFC7268A02A9",
								"kind": "group",
								"name": "issue_boards_organisms_list6",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 1776,
									"y": 442,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-ndczmje3.png",
									"frame": {
										"x": 1776,
										"y": 442,
										"width": 240,
										"height": 97
									}
								},
								"children": [
									{
										"objectId": "7A02CEC3-9B77-4043-9DF3-EF695E86624F",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible6",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 1776,
											"y": 442,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-n0ewmknf.png",
											"frame": {
												"x": 1776,
												"y": 442,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "0089244D-B996-44DA-8B56-950C69EF9571",
												"kind": "group",
												"name": "Group_128",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1791,
													"y": 502,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mda4oti0.png",
													"frame": {
														"x": 1791,
														"y": 502,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "775B2CCC-3CEA-47F1-A979-C657F4BCC3DD",
														"kind": "group",
														"name": "Group_108",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1791.625,
															"y": 502,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nzc1qjjd.png",
															"frame": {
																"x": 1791.625,
																"y": 502,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "92EE68AE-5687-4071-A076-C9843ECB3C28",
																"kind": "group",
																"name": "icon_check_s8",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1791.625,
																	"y": 502,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "8B42BC91-4A92-414E-B0CA-D3D8D5B6D160",
																		"kind": "group",
																		"name": "Group8",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1791.625,
																			"y": 502,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-oei0mkjd.png",
																			"frame": {
																				"x": 1791.625,
																				"y": 502,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "200BB4F8-CC28-41E6-A8D4-CD8C7251CC02",
												"kind": "text",
												"name": "title4",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 1792,
													"y": 462,
													"width": 62,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-test",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-mjawqki0.png",
													"frame": {
														"x": 1792,
														"y": 462,
														"width": 62,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "7D6C5575-C4A8-4078-BA98-BF97ED8B0469",
								"kind": "group",
								"name": "issue_boards_organisms_list7",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 1520,
									"y": 442,
									"width": 240,
									"height": 194
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-n0q2qzu1.png",
									"frame": {
										"x": 1520,
										"y": 442,
										"width": 240,
										"height": 194
									}
								},
								"children": [
									{
										"objectId": "9D470E76-FFAB-449C-A983-C2CFBFC1BB26",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible7",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 1520,
											"y": 442,
											"width": 240,
											"height": 182
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-ouq0nzbf.png",
											"frame": {
												"x": 1520,
												"y": 442,
												"width": 240,
												"height": 182
											}
										},
										"children": [
											{
												"objectId": "44EADE72-6997-4531-AF74-A357D2A27BE9",
												"kind": "group",
												"name": "Group_129",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1535,
													"y": 600,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-ndrfqurf.png",
													"frame": {
														"x": 1535,
														"y": 600,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "A7EC7BB9-177D-45AA-B67F-1CBEE4AA2D8A",
														"kind": "group",
														"name": "Group_109",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1535.625,
															"y": 600,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-qtdfqzdc.png",
															"frame": {
																"x": 1535.625,
																"y": 600,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "B7A05185-590B-40A2-9EA3-B0F24BE5A586",
																"kind": "group",
																"name": "icon_check_s9",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1535.625,
																	"y": 600,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "B7559E53-499B-4D0F-8547-DAF96AF7BC05",
																		"kind": "group",
																		"name": "Group9",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1535.625,
																			"y": 600,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-qjc1ntlf.png",
																			"frame": {
																				"x": 1535.625,
																				"y": 600,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "68977B32-35FC-44CB-9D08-91F57F6F1ED1",
												"kind": "group",
												"name": "Group_1210",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1520,
													"y": 551,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-njg5nzdc.png",
													"frame": {
														"x": 1520,
														"y": 551,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "538FF4BF-72CF-4C8A-8FCF-DE96403DC259",
														"kind": "group",
														"name": "Group_1010",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1535.625,
															"y": 551,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-ntm4rky0.png",
															"frame": {
																"x": 1535.625,
																"y": 551,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "80C09564-8EA4-4205-AFA6-A6EBC4B21BB1",
																"kind": "group",
																"name": "icon_check_s10",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1535.625,
																	"y": 551,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "29B36D43-4AC6-4BB4-94A5-B5A8AB5A1A1C",
																		"kind": "group",
																		"name": "Group10",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1535.625,
																			"y": 551,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-mjlcmzze.png",
																			"frame": {
																				"x": 1535.625,
																				"y": 551,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "48C63986-DB53-47E3-8E51-07157261D3BD",
												"kind": "group",
												"name": "Group_1211",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1520,
													"y": 502,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-ndhdnjm5.png",
													"frame": {
														"x": 1520,
														"y": 502,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "C69E2EFF-50F6-48A5-BB31-F5D98BCAE6C5",
														"kind": "group",
														"name": "Group_1011",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1535.625,
															"y": 502,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-qzy5rtjf.png",
															"frame": {
																"x": 1535.625,
																"y": 502,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "223B3004-78C3-4F8C-BBCF-99526BCEB416",
																"kind": "group",
																"name": "icon_check_s11",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1535.625,
																	"y": 502,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "0F32C317-8B41-475E-AB87-95C3A2C142F9",
																		"kind": "group",
																		"name": "Group11",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1535.625,
																			"y": 502,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-meyzmkmz.png",
																			"frame": {
																				"x": 1535.625,
																				"y": 502,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "FF80C69F-7C05-4A7F-8B08-C1590360014D",
												"kind": "text",
												"name": "title5",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 1536,
													"y": 462,
													"width": 35,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Tests",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-rky4mem2.png",
													"frame": {
														"x": 1536,
														"y": 462,
														"width": 35,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "64D903ED-9F3F-4EA7-8E04-EAB669F9FA6A",
						"kind": "group",
						"name": "dsloading",
						"originalName": "dsloading",
						"maskFrame": null,
						"layerFrame": {
							"x": 1503,
							"y": 442,
							"width": 257,
							"height": 97
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-dsloading-njreotaz.png",
							"frame": {
								"x": 1503,
								"y": 442,
								"width": 257,
								"height": 97
							}
						},
						"children": [
							{
								"objectId": "B2ECF3D3-A953-462F-95A7-1E04636953F1",
								"kind": "group",
								"name": "issue_boards_organisms_list8",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 1520,
									"y": 442,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "E3D42065-C454-4F8E-B367-B8F86071CEF6",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible8",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 1520,
											"y": 442,
											"width": 240,
											"height": 48
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-rtnendiw.png",
											"frame": {
												"x": 1520,
												"y": 442,
												"width": 240,
												"height": 48
											}
										},
										"children": [
											{
												"objectId": "E55A50FB-D606-4BD7-B371-668E8681A215",
												"kind": "group",
												"name": "background2",
												"originalName": "background",
												"maskFrame": null,
												"layerFrame": {
													"x": 1520,
													"y": 442,
													"width": 240,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-background-rtu1qtuw.png",
													"frame": {
														"x": 1520,
														"y": 442,
														"width": 240,
														"height": 48
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "90C09D61-F17B-4E75-BD42-03AADDDF1D74",
										"kind": "group",
										"name": "Group_1012",
										"originalName": "Group 10",
										"maskFrame": null,
										"layerFrame": {
											"x": 1536,
											"y": 502,
											"width": 208,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "FC3DC2E6-2B4D-4EB6-B878-4A3EF62E830C",
												"kind": "group",
												"name": "icon_check_s12",
												"originalName": "icon-check-s",
												"maskFrame": null,
												"layerFrame": {
													"x": 1536,
													"y": 502,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "09A2FF3E-DABE-40E7-99DA-E25E36CB42B9",
														"kind": "group",
														"name": "Group12",
														"originalName": "Group",
														"maskFrame": null,
														"layerFrame": {
															"x": 1536,
															"y": 502,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group-mdlbmkzg.png",
															"frame": {
																"x": 1536,
																"y": 502,
																"width": 208,
																"height": 24
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "8A957D7A-81D0-4478-A234-F7F313B0AD3C",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners1",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": 1520,
											"y": 442,
											"width": 240,
											"height": 97
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-oee5ntde.png",
											"frame": {
												"x": 1520,
												"y": 442,
												"width": 240,
												"height": 97
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "FED3EE96-9366-45B2-A5E0-7B988B4573E3",
						"kind": "group",
						"name": "graph2",
						"originalName": "graph2",
						"maskFrame": null,
						"layerFrame": {
							"x": 240,
							"y": 358,
							"width": 1264,
							"height": 194
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-graph2-rkvem0vf.png",
							"frame": {
								"x": 240,
								"y": 358,
								"width": 1264,
								"height": 194
							}
						},
						"children": [
							{
								"objectId": "06DAA97F-CEB8-478A-BEA7-E9292BB81D51",
								"kind": "group",
								"name": "usclick",
								"originalName": "usclick",
								"maskFrame": null,
								"layerFrame": {
									"x": 240,
									"y": 358,
									"width": 240,
									"height": 84
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "3A76A006-AC81-413C-8DF2-FD51FD2797BE",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible9",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 256,
											"y": 370,
											"width": 202,
											"height": 55
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-m0e3nkew.png",
											"frame": {
												"x": 256,
												"y": 370,
												"width": 202,
												"height": 55
											}
										},
										"children": [
											{
												"objectId": "735E59C4-BA04-43A2-B193-98E4F9E33FDC",
												"kind": "group",
												"name": "Group_1212",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 256,
													"y": 370,
													"width": 202,
													"height": 55
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-nzm1rtu5.png",
													"frame": {
														"x": 256,
														"y": 370,
														"width": 202,
														"height": 55
													}
												},
												"children": [
													{
														"objectId": "7497D420-264F-45FE-BE29-1426DBF2DFB9",
														"kind": "group",
														"name": "Group_1013",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 255.625,
															"y": 370,
															"width": 203,
															"height": 55
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nzq5n0q0.png",
															"frame": {
																"x": 255.625,
																"y": 370,
																"width": 203,
																"height": 55
															}
														},
														"children": [
															{
																"objectId": "36DED5FD-07F2-4173-B913-EE00357087C7",
																"kind": "group",
																"name": "uschevron",
																"originalName": "uschevron",
																"maskFrame": null,
																"layerFrame": {
																	"x": 452.625,
																	"y": 378,
																	"width": 6,
																	"height": 8
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-uschevron-mzzeruq1.png",
																	"frame": {
																		"x": 452.625,
																		"y": 378,
																		"width": 6,
																		"height": 8
																	}
																},
																"children": []
															}
														]
													}
												]
											}
										]
									},
									{
										"objectId": "E3847608-2BA0-4EE7-8A2F-1924829CBBFB",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners2",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": 240,
											"y": 358,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-rtm4ndc2.png",
											"frame": {
												"x": 240,
												"y": 358,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "C775B066-80A2-4D29-8321-F309203DCDB5",
												"kind": "group",
												"name": "usclickbghover",
												"originalName": "usclickbghover",
												"maskFrame": null,
												"layerFrame": {
													"x": 240,
													"y": 358,
													"width": 240,
													"height": 84
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-usclickbghover-qzc3nuiw.png",
													"frame": {
														"x": 240,
														"y": 358,
														"width": 240,
														"height": 84
													}
												},
												"children": []
											},
											{
												"objectId": "6F0DDD5A-BD33-49C3-B419-2C62D38138B9",
												"kind": "group",
												"name": "usclickbg",
												"originalName": "usclickbg",
												"maskFrame": null,
												"layerFrame": {
													"x": 240,
													"y": 358,
													"width": 240,
													"height": 84
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-usclickbg-nkywrere.png",
													"frame": {
														"x": 240,
														"y": 358,
														"width": 240,
														"height": 84
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "FC06A9C3-0C03-45DB-829A-DCC1B716F004",
								"kind": "group",
								"name": "issue_boards_organisms_list9",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 1264,
									"y": 358,
									"width": 240,
									"height": 169
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-rkmwnke5.png",
									"frame": {
										"x": 1264,
										"y": 358,
										"width": 240,
										"height": 169
									}
								},
								"children": [
									{
										"objectId": "073A0355-8142-481D-BD92-B35125AB5CFA",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible10",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 1264,
											"y": 374,
											"width": 240,
											"height": 153
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-mdczqtaz.png",
											"frame": {
												"x": 1264,
												"y": 374,
												"width": 240,
												"height": 153
											}
										},
										"children": [
											{
												"objectId": "17C34B92-7E4A-4694-9078-8C53235920D8",
												"kind": "group",
												"name": "dsclick",
												"originalName": "dsclick",
												"maskFrame": null,
												"layerFrame": {
													"x": 1265,
													"y": 442,
													"width": 238,
													"height": 85
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-dsclick-mtddmzrc.png",
													"frame": {
														"x": 1265,
														"y": 442,
														"width": 238,
														"height": 85
													}
												},
												"children": [
													{
														"objectId": "33969AB0-812D-40B2-A130-37B9F6BA46FA",
														"kind": "group",
														"name": "Group_1014",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1280,
															"y": 455,
															"width": 203,
															"height": 55
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-mzm5njlb.png",
															"frame": {
																"x": 1280,
																"y": 455,
																"width": 203,
																"height": 55
															}
														},
														"children": [
															{
																"objectId": "B441D0B5-72D2-44FE-B115-F5C15772E418",
																"kind": "group",
																"name": "dschevron",
																"originalName": "dschevron",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1478,
																	"y": 463,
																	"width": 5,
																	"height": 8
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-dschevron-qjq0muqw.png",
																	"frame": {
																		"x": 1478,
																		"y": 463,
																		"width": 5,
																		"height": 8
																	}
																},
																"children": []
															},
															{
																"objectId": "A747803B-1631-4FD9-9C6D-DDB49ED4C947",
																"kind": "group",
																"name": "icon_check_s13",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1280,
																	"y": 455,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "D3B37A73-1366-4926-BF80-391A3FDF330E",
																		"kind": "group",
																		"name": "Group13",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1280,
																			"y": 455,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-rdncmzdb.png",
																			"frame": {
																				"x": 1280,
																				"y": 455,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													},
													{
														"objectId": "6DF72157-289D-4A56-964B-712D8A22A2A2",
														"kind": "group",
														"name": "jlkjasd",
														"originalName": "jlkjasd",
														"maskFrame": null,
														"layerFrame": {
															"x": 1265,
															"y": 442,
															"width": 238,
															"height": 85
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-jlkjasd-nkrgnzix.png",
															"frame": {
																"x": 1265,
																"y": 442,
																"width": 238,
																"height": 85
															}
														},
														"children": [
															{
																"objectId": "C513D0F6-F976-4C67-85DD-688B4B02F6F1",
																"kind": "group",
																"name": "dsclickbghover",
																"originalName": "dsclickbghover",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1265,
																	"y": 442.5,
																	"width": 238,
																	"height": 85
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-dsclickbghover-qzuxm0qw.png",
																	"frame": {
																		"x": 1265,
																		"y": 442.5,
																		"width": 238,
																		"height": 85
																	}
																},
																"children": []
															},
															{
																"objectId": "3435243A-D3F0-4CEF-8E35-4D247647A382",
																"kind": "group",
																"name": "dsclickbg",
																"originalName": "dsclickbg",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1265,
																	"y": 442.5,
																	"width": 238,
																	"height": 85
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-dsclickbg-mzqznti0.png",
																	"frame": {
																		"x": 1265,
																		"y": 442.5,
																		"width": 238,
																		"height": 85
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "D71DAB19-86DB-4412-B507-5CD3D0BEDC57",
												"kind": "group",
												"name": "Group_1213",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1264,
													"y": 374,
													"width": 240,
													"height": 69
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-rdcxrefc.png",
													"frame": {
														"x": 1264,
														"y": 374,
														"width": 240,
														"height": 69
													}
												},
												"children": [
													{
														"objectId": "F7AE8AA7-7AB9-4883-852A-D3504192AEC9",
														"kind": "group",
														"name": "Group_1015",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1263.625,
															"y": 374,
															"width": 240,
															"height": 69
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-rjdbrthb.png",
															"frame": {
																"x": 1263.625,
																"y": 374,
																"width": 240,
																"height": 69
															}
														},
														"children": [
															{
																"objectId": "AFE43A7E-A7C0-4F7E-988E-7EFFC5C50946",
																"kind": "group",
																"name": "global_components_other_avatar_1",
																"originalName": "global-components/other/avatar/",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1278.625,
																	"y": 374,
																	"width": 25,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "A479236D-3C8E-4D68-90AE-369018EB48F7",
																		"kind": "group",
																		"name": "global_components_other_avatar_modifier_user_avatar1",
																		"originalName": "global-components/other/avatar/modifier/user-avatar",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1279,
																			"y": 374,
																			"width": 24,
																			"height": 24
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "A2B59FFC-913C-4D38-9BB9-ED5A4013E46B",
																				"kind": "group",
																				"name": "group_icon1",
																				"originalName": "group-icon",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 1279,
																					"y": 374,
																					"width": 25,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-group_icon-qtjcntlg.png",
																					"frame": {
																						"x": 1279,
																						"y": 374,
																						"width": 25,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	}
																]
															},
															{
																"objectId": "AE0B0582-4BFF-46D1-92B9-0D2F9A80B785",
																"kind": "group",
																"name": "icon_check_s14",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1279.625,
																	"y": 374,
																	"width": 25,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "5329E263-D2D5-4BF0-97C7-4AEA16F01B9D",
																		"kind": "group",
																		"name": "Group14",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1280,
																			"y": 374,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-ntmyouuy.png",
																			"frame": {
																				"x": 1280,
																				"y": 374,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											}
										]
									}
								]
							},
							{
								"objectId": "774B8E2C-9838-475D-A5DE-6AB48ABA78A8",
								"kind": "group",
								"name": "issue_boards_organisms_list10",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 1008,
									"y": 358,
									"width": 240,
									"height": 96
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-nzc0qjhf.png",
									"frame": {
										"x": 1008,
										"y": 358,
										"width": 240,
										"height": 96
									}
								},
								"children": [
									{
										"objectId": "EB3FA05F-A519-44E2-BD32-D06852034F92",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible11",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 1008,
											"y": 358,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-ruizrkew.png",
											"frame": {
												"x": 1008,
												"y": 358,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "A6EEE701-99E2-4BA4-A3EF-B12442AD8FAB",
												"kind": "group",
												"name": "Group_1214",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 1023,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-qtzfruu3.png",
													"frame": {
														"x": 1023,
														"y": 418,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "30CCB396-39A1-49FC-BC09-0CD0D26AA9D0",
														"kind": "group",
														"name": "Group_1016",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 1023.625,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-mzbdq0iz.png",
															"frame": {
																"x": 1023.625,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "59AA8632-947B-4EEB-9FD3-BB62A6BD3CBC",
																"kind": "group",
																"name": "icon_check_s15",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 1023.625,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "989FF0DA-CD59-4354-AFED-0158F9ABB849",
																		"kind": "group",
																		"name": "Group15",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 1023.625,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-otg5rkyw.png",
																			"frame": {
																				"x": 1023.625,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "3502FE78-66A7-4BB7-B82D-6F71E566AE8C",
												"kind": "text",
												"name": "title6",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 1024,
													"y": 378,
													"width": 89,
													"height": 13
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-cleanup",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-mzuwmkzf.png",
													"frame": {
														"x": 1024,
														"y": 378,
														"width": 89,
														"height": 13
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "B4EECAA6-D237-43EA-A54B-99C9CCD4A58E",
								"kind": "group",
								"name": "issue_boards_organisms_list11",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 752,
									"y": 358,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-qjrfrunb.png",
									"frame": {
										"x": 752,
										"y": 358,
										"width": 240,
										"height": 97
									}
								},
								"children": [
									{
										"objectId": "CE942AC7-D171-48FC-AB23-8A719BAD0502",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible12",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 752,
											"y": 358,
											"width": 240,
											"height": 84
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-q0u5ndjb.png",
											"frame": {
												"x": 752,
												"y": 358,
												"width": 240,
												"height": 84
											}
										},
										"children": [
											{
												"objectId": "0491F7F5-B7AD-4E43-B335-CD3CBA568F55",
												"kind": "group",
												"name": "Group_1215",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 767,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-mdq5muy3.png",
													"frame": {
														"x": 767,
														"y": 418,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "729549D8-6376-4D54-BC2C-A1F9313474CA",
														"kind": "group",
														"name": "Group_1017",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 767.625,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nzi5ntq5.png",
															"frame": {
																"x": 767.625,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "9B94250A-2D7A-4242-A503-A7E7E22B5BDC",
																"kind": "group",
																"name": "icon_check_s16",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 767.625,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "37E491D4-128F-49B4-A58B-082C5BFBA3D7",
																		"kind": "group",
																		"name": "Group16",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 767.625,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-mzdfndkx.png",
																			"frame": {
																				"x": 767.625,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "69D45914-FEFF-4ACB-9ED2-37C6179AA98E",
												"kind": "text",
												"name": "title7",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 768,
													"y": 378,
													"width": 62,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Post-test",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-njlendu5.png",
													"frame": {
														"x": 768,
														"y": 378,
														"width": 62,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "52580707-A045-4073-996B-B5C8435DAADC",
								"kind": "group",
								"name": "issue_boards_organisms_list12",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 496,
									"y": 358,
									"width": 240,
									"height": 194
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-issue_boards_organisms_list-nti1oda3.png",
									"frame": {
										"x": 496,
										"y": 358,
										"width": 240,
										"height": 194
									}
								},
								"children": [
									{
										"objectId": "9094A27B-8032-43DB-A3AB-27E1403AB4DA",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible13",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 496,
											"y": 358,
											"width": 240,
											"height": 182
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-ota5neey.png",
											"frame": {
												"x": 496,
												"y": 358,
												"width": 240,
												"height": 182
											}
										},
										"children": [
											{
												"objectId": "A38ABF72-51C8-42B1-A402-1F470FCC12C6",
												"kind": "group",
												"name": "Group_1216",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 511,
													"y": 516,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-qtm4qujg.png",
													"frame": {
														"x": 511,
														"y": 516,
														"width": 208,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "E46EB9A0-40DD-4E3B-9931-406F4F90C175",
														"kind": "group",
														"name": "Group_1018",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 511.625,
															"y": 516,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-rtq2rui5.png",
															"frame": {
																"x": 511.625,
																"y": 516,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "6E356AFB-737D-4EBD-A31D-1FF278841049",
																"kind": "group",
																"name": "icon_check_s17",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 511.625,
																	"y": 516,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "113DB4E2-6C3D-4EBF-9F0B-FA878EF6804D",
																		"kind": "group",
																		"name": "Group17",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511.625,
																			"y": 516,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-mtezrei0.png",
																			"frame": {
																				"x": 511.625,
																				"y": 516,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "951427DA-9AA0-4A1B-BD64-CA4317D89648",
												"kind": "group",
												"name": "Group_1217",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 496,
													"y": 467,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-otuxndi3.png",
													"frame": {
														"x": 496,
														"y": 467,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "4F3CB3FA-074D-4E25-9428-7C97C4017BA3",
														"kind": "group",
														"name": "Group_1019",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 511.625,
															"y": 467,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-neyzq0iz.png",
															"frame": {
																"x": 511.625,
																"y": 467,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "A4ED5671-0C4D-4166-9296-2015B11032B3",
																"kind": "group",
																"name": "icon_check_s18",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 511.625,
																	"y": 467,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "0B3B94BF-84DD-4C25-BA9D-82015A259C2E",
																		"kind": "group",
																		"name": "Group18",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511.625,
																			"y": 467,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-meizqjk0.png",
																			"frame": {
																				"x": 511.625,
																				"y": 467,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "D2E12CD5-C4D9-4896-BD57-AF743DA11E70",
												"kind": "group",
												"name": "Group_1218",
												"originalName": "Group 12",
												"maskFrame": null,
												"layerFrame": {
													"x": 496,
													"y": 418,
													"width": 240,
													"height": 37
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_12-rdjfmtjd.png",
													"frame": {
														"x": 496,
														"y": 418,
														"width": 240,
														"height": 37
													}
												},
												"children": [
													{
														"objectId": "6DDBF24A-9928-47BA-A831-1D164013E700",
														"kind": "group",
														"name": "Group_1020",
														"originalName": "Group 10",
														"maskFrame": null,
														"layerFrame": {
															"x": 511.625,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_10-nkreqkyy.png",
															"frame": {
																"x": 511.625,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": [
															{
																"objectId": "D187023D-B582-4F5A-AF6C-24431CC6E50F",
																"kind": "group",
																"name": "icon_check_s19",
																"originalName": "icon-check-s",
																"maskFrame": null,
																"layerFrame": {
																	"x": 511.625,
																	"y": 418,
																	"width": 24,
																	"height": 24
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "4B4C3738-4D35-432A-9231-53D5A74F1A91",
																		"kind": "group",
																		"name": "Group19",
																		"originalName": "Group",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511.625,
																			"y": 418,
																			"width": 24,
																			"height": 24
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group-nei0qzm3.png",
																			"frame": {
																				"x": 511.625,
																				"y": 418,
																				"width": 24,
																				"height": 24
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													}
												]
											},
											{
												"objectId": "8EF7B5F7-B8E6-4B5D-9515-8334CD0A14CE",
												"kind": "text",
												"name": "title8",
												"originalName": "title",
												"maskFrame": null,
												"layerFrame": {
													"x": 512,
													"y": 378,
													"width": 35,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Tests",
													"css": [
														"font-family: HelveticaNeue-Bold;",
														"font-size: 14px;",
														"color: #2E2E2E;",
														"text-align: left;",
														"line-height: 16px;"
													]
												},
												"image": {
													"path": "images/Layer-title-oevgn0i1.png",
													"frame": {
														"x": 512,
														"y": 378,
														"width": 35,
														"height": 11
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "E216B468-68C3-482D-A820-AD11C7721439",
						"kind": "group",
						"name": "graph",
						"originalName": "graph",
						"maskFrame": null,
						"layerFrame": {
							"x": 240,
							"y": 358,
							"width": 752,
							"height": 97
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-graph-rtixnki0.png",
							"frame": {
								"x": 240,
								"y": 358,
								"width": 752,
								"height": 97
							}
						},
						"children": [
							{
								"objectId": "6F43A231-B395-4111-A98D-4A0C71C76C3F",
								"kind": "group",
								"name": "issue_boards_organisms_list13",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 496,
									"y": 358,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "0FBA8A3D-55F0-4EFB-89AC-109D2A4F45A5",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible14",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 496,
											"y": 358,
											"width": 240,
											"height": 48
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "CA484F58-3BEE-4E57-816E-A15B93F8D613",
												"kind": "group",
												"name": "background3",
												"originalName": "background",
												"maskFrame": null,
												"layerFrame": {
													"x": 496,
													"y": 358,
													"width": 240,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-background-q0e0odrg.png",
													"frame": {
														"x": 496,
														"y": 358,
														"width": 240,
														"height": 48
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "38CAF075-4ADA-461F-A223-7B3D6DFF58C2",
										"kind": "group",
										"name": "Group_1021",
										"originalName": "Group 10",
										"maskFrame": null,
										"layerFrame": {
											"x": 512,
											"y": 418,
											"width": 208,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "EA3E394B-7F0A-4A37-900D-10D54A7B907D",
												"kind": "group",
												"name": "icon_check_s20",
												"originalName": "icon-check-s",
												"maskFrame": null,
												"layerFrame": {
													"x": 512,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "2D3AE029-D8B9-4543-B5A8-ACBA35A890F0",
														"kind": "group",
														"name": "Group20",
														"originalName": "Group",
														"maskFrame": null,
														"layerFrame": {
															"x": 512,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group-mkqzquuw.png",
															"frame": {
																"x": 512,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "610447E6-2643-4E99-9647-02D811E023A6",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners3",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": 496,
											"y": 358,
											"width": 240,
											"height": 97
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-njewndq3.png",
											"frame": {
												"x": 496,
												"y": 358,
												"width": 240,
												"height": 97
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "BE03957D-2E7C-4FA2-B14B-921F29BEF4C7",
								"kind": "group",
								"name": "issue_boards_organisms_list14",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 752,
									"y": 358,
									"width": 240,
									"height": 97
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "B1BD8387-AF63-4599-B97C-10E32FE164D4",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible15",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 752,
											"y": 358,
											"width": 240,
											"height": 48
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-qjfcrdgz.png",
											"frame": {
												"x": 752,
												"y": 358,
												"width": 240,
												"height": 48
											}
										},
										"children": [
											{
												"objectId": "ECE3EC5E-D82B-4065-9322-03A85C89B8FD",
												"kind": "group",
												"name": "background4",
												"originalName": "background",
												"maskFrame": null,
												"layerFrame": {
													"x": 752,
													"y": 358,
													"width": 240,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-background-runfm0vd.png",
													"frame": {
														"x": 752,
														"y": 358,
														"width": 240,
														"height": 48
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "DC58CEE3-CE49-45B7-AAB6-CB622E949512",
										"kind": "group",
										"name": "Group_1022",
										"originalName": "Group 10",
										"maskFrame": null,
										"layerFrame": {
											"x": 768,
											"y": 418,
											"width": 208,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "76A10113-0046-4068-B4F9-24755DEF2E02",
												"kind": "group",
												"name": "icon_check_s21",
												"originalName": "icon-check-s",
												"maskFrame": null,
												"layerFrame": {
													"x": 768,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "0AEDDD16-5F52-481C-9FF5-88D583287C55",
														"kind": "group",
														"name": "Group21",
														"originalName": "Group",
														"maskFrame": null,
														"layerFrame": {
															"x": 768,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group-meffrere.png",
															"frame": {
																"x": 768,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "45B72C76-A8B6-42C2-9249-A4407481D8FD",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners4",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": 752,
											"y": 358,
											"width": 240,
											"height": 97
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-ndvcnzjd.png",
											"frame": {
												"x": 752,
												"y": 358,
												"width": 240,
												"height": 97
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "3ADC6FFB-479C-489B-9F58-7E3A0E779F4E",
								"kind": "group",
								"name": "issue_boards_organisms_list15",
								"originalName": "issue-boards/organisms/list",
								"maskFrame": null,
								"layerFrame": {
									"x": 240,
									"y": 358,
									"width": 240,
									"height": 84
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "B5BBC115-5F55-4DBE-8FD1-43DCB089DDA5",
										"kind": "group",
										"name": "issue_boards_molecules_list_header_collapsible16",
										"originalName": "issue-boards/molecules/list-header--collapsible",
										"maskFrame": null,
										"layerFrame": {
											"x": 240,
											"y": 358,
											"width": 240,
											"height": 48
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-issue_boards_molecules_list_header_collapsible-qjvcqkmx.png",
											"frame": {
												"x": 240,
												"y": 358,
												"width": 240,
												"height": 48
											}
										},
										"children": [
											{
												"objectId": "D6A68E7B-102F-4EEC-9829-AA8C5EB792F2",
												"kind": "group",
												"name": "background5",
												"originalName": "background",
												"maskFrame": null,
												"layerFrame": {
													"x": 240,
													"y": 358,
													"width": 240,
													"height": 48
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-background-rdzbnjhf.png",
													"frame": {
														"x": 240,
														"y": 358,
														"width": 240,
														"height": 48
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "CE014874-9A4E-49FE-B1A9-5A4A0DBB3287",
										"kind": "group",
										"name": "Group_1023",
										"originalName": "Group 10",
										"maskFrame": null,
										"layerFrame": {
											"x": 256,
											"y": 418,
											"width": 208,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "2A206D9E-07B4-4612-9F99-5C530C4FD672",
												"kind": "group",
												"name": "icon_check_s22",
												"originalName": "icon-check-s",
												"maskFrame": null,
												"layerFrame": {
													"x": 256,
													"y": 418,
													"width": 208,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "0DCCCDB7-CA25-4697-A43B-3D5CAD53A478",
														"kind": "group",
														"name": "Group22",
														"originalName": "Group",
														"maskFrame": null,
														"layerFrame": {
															"x": 256,
															"y": 418,
															"width": 208,
															"height": 24
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group-merdq0ne.png",
															"frame": {
																"x": 256,
																"y": 418,
																"width": 208,
																"height": 24
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "0EF23126-8E3F-4A19-9772-DA2CFE2CA6EA",
										"kind": "group",
										"name": "global_components_backgrounds_grey_box_light_all_rounded_corners5",
										"originalName": "global-components/backgrounds/grey/box/light--all-rounded-corners",
										"maskFrame": null,
										"layerFrame": {
											"x": 240,
											"y": 358,
											"width": 240,
											"height": 97
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-global_components_backgrounds_grey_box_light_all_rounded_corners-mevgmjmx.png",
											"frame": {
												"x": 240,
												"y": 358,
												"width": 240,
												"height": 97
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "F13521CE-C98D-447B-A64D-CC6711EB0D36",
				"kind": "group",
				"name": "background",
				"originalName": "background",
				"maskFrame": null,
				"layerFrame": {
					"x": -3,
					"y": -1,
					"width": 1772,
					"height": 345
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-background-rjezntix.png",
					"frame": {
						"x": -3,
						"y": -1,
						"width": 1772,
						"height": 345
					}
				},
				"children": []
			}
		]
	}
]